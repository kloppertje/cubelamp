#include <Arduino.h>
#include <Streaming.h>
#include "button.h"
#include "capTouch.h"
// #include <ArduinoOTA.h>
// #include "wifiConfig.h"
#include <esp_now.h>
#include <WiFi.h>

uint8_t broadcastAddress[] = {0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF};

// #define PIN_TOUCH1 2
// #define PIN_TOUCH2 4
#define PIN_TOUCH1 TOUCH_PAD_NUM6
#define PIN_TOUCH2 TOUCH_PAD_NUM5
#define PIN_LED1 13
#define PIN_LED2 15

capSensor c1, c2;
button b1, b2;

uint8_t messageReceived = 0;

void touchTask(void* parameter) {
  uint16_t touch1, touch2;
    while (1) {
      touch_pad_read(PIN_TOUCH1, &touch1);
      touch_pad_read(PIN_TOUCH2, &touch2);
        capTouch_update(&c1, touch1);
        capTouch_update(&c2, touch2);
        // capTouch_update(&c1, touchRead(PIN_TOUCH1));
        // capTouch_update(&c2, touchRead(PIN_TOUCH2));
        button_update(&b1, c1.press);
        button_update(&b2, c2.press);
        vTaskDelay(1);
        // delay(10);
    }
}

// callback when data is sent from Master to Slave
void OnDataSent(const uint8_t *mac_addr, esp_now_send_status_t status) {
	char macStr[18];
	snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
		mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
	Serial.print("Last Packet Sent to: "); Serial.println(macStr);
	Serial.print("Last Packet Send Status: "); Serial.println(status == ESP_NOW_SEND_SUCCESS ? "Delivery Success" : "Delivery Fail");
}

// callback when data is recv from Master
void OnDataRecv(const uint8_t *mac_addr, const uint8_t *data, int data_len) {
	char macStr[18];
	snprintf(macStr, sizeof(macStr), "%02x:%02x:%02x:%02x:%02x:%02x",
		mac_addr[0], mac_addr[1], mac_addr[2], mac_addr[3], mac_addr[4], mac_addr[5]);
	Serial.print("Last Packet Recv from: "); Serial.println(macStr);
	Serial.print("Last Packet Recv Data: "); Serial.println(*data);
	Serial.println("");

  messageReceived = *data;
}


void setup() {
    Serial.begin(115200);

    capTouch_init(&c1, 60);
    capTouch_init(&c2, 60);
    button_init(&b1, 0);
    button_init(&b2, 0);

    pinMode(PIN_LED1, OUTPUT);
    pinMode(PIN_LED2, OUTPUT);

    xTaskCreatePinnedToCore(
        touchTask,   /* Function to implement the task */
        "touchTask", /* Name of the task */
        10000,       /* Stack size in words */
        NULL,        /* Task input parameter */
        0,           /* Priority of the task */
        NULL,        /* Task handle. */
        1);          /* Core where the task should run */

    touch_pad_init();
    touch_pad_config(PIN_TOUCH1, 0);
    touch_pad_config(PIN_TOUCH2, 0);

    touch_pad_set_voltage(TOUCH_HVOLT_2V7, TOUCH_LVOLT_0V5, TOUCH_HVOLT_ATTEN_1V);
    // touch_pad_set_voltage(TOUCH_HVOLT_2V4, TOUCH_LVOLT_0V8, TOUCH_HVOLT_ATTEN_1V5);
    // touch_pad_set_cnt_mode(TOUCH_PAD_NUM0, TOUCH_PAD_SLOPE_7, TOUCH_PAD_TIE_OPT_LOW);

    // #define TOUCH_PAD_SLEEP_CYCLE_DEFAULT   (0x1000)  /*!<The timer frequency is RTC_SLOW_CLK (can be 150k or 32k depending on the options), max value is 0xffff */
    // #define TOUCH_PAD_MEASURE_CYCLE_DEFAULT (0x7fff)  /*!<The timer frequency is 8Mhz, the max value is 0x7fff */
    // touch_pad_set_meas_time(0x3000, 0x3fff);

    // Init esp now
    WiFi.mode(WIFI_STA);    
    Serial.println(WiFi.macAddress());
    if (esp_now_init() != ESP_OK) {
      Serial.println("Error initializing ESP-NOW");
      return;
    }

    esp_now_register_send_cb(OnDataSent);
    esp_now_register_recv_cb(OnDataRecv);

    esp_now_peer_info_t peerInfo;
    memcpy(peerInfo.peer_addr, broadcastAddress, 6);
    peerInfo.channel = 0;  
    peerInfo.encrypt = false;

    if (esp_now_add_peer(&peerInfo) != ESP_OK){
      Serial.println("Failed to add peer");
      return;
    }

  // WiFi.mode(WIFI_STA);
  // WiFi.begin(WIFI_SSID, WIFI_KEY);
  // while (WiFi.waitForConnectResult() != WL_CONNECTED) {
  //   Serial.println("Connection Failed! Rebooting...");
  //   delay(3000);
  //   ESP.restart();
  // }

  // ArduinoOTA.setHostname("cubelamp");

  // ArduinoOTA
  //   .onStart([]() {
  //     String type;
  //     if (ArduinoOTA.getCommand() == U_FLASH)
  //       type = "sketch";
  //     else // U_SPIFFS
  //       type = "filesystem";

  //     // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
  //     Serial.println("Start updating " + type);
  //   })
  //   .onEnd([]() {
  //     Serial.println("\nEnd");
  //   })
  //   .onProgress([](unsigned int progress, unsigned int total) {
  //     Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  //   })
  //   .onError([](ota_error_t error) {
  //     Serial.printf("Error[%u]: ", error);
  //     if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
  //     else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
  //     else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
  //     else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
  //     else if (error == OTA_END_ERROR) Serial.println("End Failed");
  //   });

  // ArduinoOTA.begin();

  // Serial.println("Ready");
  // Serial.print("IP address: ");
  // Serial.println(WiFi.localIP());

}

void loop() {
  static boolean led1State = 0, led2State = 0;
  // ArduinoOTA.handle();

  // pinMode(4, OUTPUT);
  // digitalWrite(4, HIGH);
  // delay(500);
  // digitalWrite(4, LOW);
  // delay(500);
  // pinMode(4, INPUT);
  // delay(500);

  // Serial << c1.ref << "\t" << c1.signal << "\t" << c1.press*400 << "\t";// << endl;
  // Serial << c2.ref << "\t" << c2.signal << "\t" << c2.press*400 << "\t" << endl;
  Serial << c1.ref << "\t" << c1.signal << "\t";// << endl;
  Serial << c2.ref << "\t" << c2.signal << "\t" << endl;

  if (b1.wasPressed) {
    b1.wasPressed = 0;
    led1State = !led1State;
    digitalWrite(PIN_LED1, led1State);

    uint8_t payLoad = 123;
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &payLoad, sizeof(payLoad));
  }

  if (b2.wasPressed) {
    b2.wasPressed = 0;
    led2State = !led2State;
    digitalWrite(PIN_LED2, led2State);
    
    uint8_t payLoad = 124;
    esp_err_t result = esp_now_send(broadcastAddress, (uint8_t *) &payLoad, sizeof(payLoad));
    if (result == ESP_OK) {
      Serial.println("Sent with success");
    }
    else {
      Serial.println("Error sending the data");
    }
  }

  if (messageReceived==123) {
    led1State = !led1State;
    digitalWrite(PIN_LED1, led1State);
    messageReceived = 0;
  }
  if (messageReceived==124) {
    led2State = !led2State;
    digitalWrite(PIN_LED2, led2State);
    messageReceived = 0;
  }
  delay(10);
}