#include "button.h"

void button_init(button *b, boolean invert) {
    b->invert = invert;
    b->state = 0;
    b->lastState = 0;
    b->wasPressed = 0;
}

void button_update(button *b, boolean newValue) {
    if (b->invert) {
        b->state = !newValue;
    } else {
        b->state = newValue;
    }

    // Reset state variables
    b->rise = 0;
    b->fall = 0;
    b->longPress = 0;
    b->longPressStart = 0;
    b->doublePress = 0;

    unsigned long currentTime = millis();

    if (b->state && !b->lastState) {  // Detect rising edge
        b->rise = 1;
        b->startTime = currentTime;
        b->wasPressed = 1;
    } else if (!b->state && b->lastState) {  // Detect falling edge
        b->fall = 1;
        b->releaseTime = currentTime;
    }

    if (b->state && (currentTime - b->startTime) > BUTTON_HOLD_TIME) {
        b->longPress = 1;
        if (b->lastLongPress == 0) {
            b->longPressStart = 1;
            b->longPressStartTime = currentTime;
        }
    }
    b->lastLongPress = b->longPress;

    if (b->rise && ((currentTime - b->releaseTime) < 300)) {
        b->doublePress = 1;
    }

    b->lastState = b->state;
}