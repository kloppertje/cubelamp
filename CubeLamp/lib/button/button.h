#ifndef _BUTTON_H_
#define _BUTTON_H_

#include <Arduino.h>

#define BUTTON_HOLD_TIME 500 // Register long press after N milliseconds


typedef struct {
  boolean state;
  boolean lastState;
  boolean rise;
  boolean fall;
  boolean longPress;
  boolean lastLongPress;
  boolean longPressStart;
  boolean wasPressed;
  unsigned long longPressStartTime;
  boolean doublePress;
  unsigned long startTime;
  unsigned long releaseTime;
  boolean invert;
} button;

void button_init(button *b, boolean invert);
void button_update(button *b, boolean newValue);

#endif