#ifndef _CONTROL_H_
#define _CONTROL_H_


#include <Arduino.h>

#define CAPTOUCH_DRIFT_PERIOD_MS 1000
#define CAPTOUCH_FILTER_LENGTH 3
#define CAPTOUCH_INVERT 1 // 0 = don't invert, 1 = invert. Invert means a press lowers the signal reading.


typedef struct {
  uint16_t ref;
  uint16_t signal;
  uint32_t signalAvg;
  int16_t delta;
  boolean press;
  boolean buffer[CAPTOUCH_FILTER_LENGTH];
  uint8_t bufferIndex;
  uint8_t bufferSum;
  unsigned long lastTime;
  uint16_t threshold;
} capSensor;

void capTouch_init(capSensor *c, uint16_t threshold);
void capTouch_update(capSensor *c, uint16_t v);
void capTouch_filter(capSensor *c, boolean newValue);



#endif