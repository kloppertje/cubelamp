#include "capTouch.h"

void capTouch_init(capSensor* c, uint16_t threshold) {
    c->press = 0;
    c->ref = 0;
    c->bufferIndex = 0;
    c->threshold = threshold;
}

void capTouch_update(capSensor* c, uint16_t v) {
    unsigned long currentTime = millis();

    c->signal = v;

    // Initial measurement
    if (c->ref == 0) {
        c->ref = v;
        c->signal = v;
    }

    // If ref is smaller than signal (with a few counts margin), equalise
    if (c->signal > (c->ref + 2)) {
        c->ref = c->signal;
    }

    // Perform positive drift compensation
    // Every 1 second
    if (currentTime - c->lastTime > CAPTOUCH_DRIFT_PERIOD_MS) {
        c->lastTime = currentTime;

        if (!c->press && c->signal < c->ref)
            c->ref--;
    }

    // Calculate delta
    c->delta = c->ref - c->signal;

    // If cap touch detected, lower threshold (hysteresis)
    boolean newPress;
    if (c->press && c->delta < (c->threshold*3)/4) {
        newPress = 0;
    } else if (!c->press && c->delta > c->threshold) {
        newPress = 1;
    } else {
        newPress = c->press;
    }

#if CAPTOUCH_FILTER_LENGTH > 1
    capTouch_filter(c, newPress);
#else
    c->press = newPress;
#endif
}

// Apply binary boolean filter, to prevent "spikes" from being detected as press.
void capTouch_filter(capSensor* c, boolean newValue) {
    c->buffer[c->bufferIndex++] = newValue;
    if (c->bufferIndex == CAPTOUCH_FILTER_LENGTH)
        c->bufferIndex = 0;

    uint8_t sum = 0;
    for (uint8_t i = 0; i < CAPTOUCH_FILTER_LENGTH; i++) {
        if (c->buffer[i]) sum++;
    }
    c->bufferSum = sum;

    if (!c->press && sum==CAPTOUCH_FILTER_LENGTH-1) {
        c->press = 1;
    } else if (c->press && sum==0) {
        c->press = 0;
    }

}